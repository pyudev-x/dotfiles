source "$HOME/.zsh/init.zsh"

# znap
# Download Znap, if it's not there yet.
[[ -r ~/Repos/znap/znap.zsh ]] ||
    git clone --depth 1 -- \
        https://github.com/marlonrichert/zsh-snap.git ~/Repos/znap
source ~/Repos/znap/znap.zsh  # Start znap

# zsh plugins
znap source zsh-users/zsh-autosuggestions
znap source zsh-users/zsh-syntax-highlighting

eval "$(starship init zsh)"
[ -f "/Users/mixer/.ghcup/env" ] && source "/Users/mixer/.ghcup/env" # ghcup-env
