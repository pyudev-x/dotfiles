local wezterm = require("wezterm")
local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- Look and feel
config.font = wezterm.font("Iosevka Nerd Font")
config.color_scheme = "Bespin (base16)"

return config
