# brew
export LIBRARY_PATH="$LIBRARY_PATH:/opt/homebrew/lib"
export PATH="$PATH:/opt/homebrew/bin"
export CPATH="$CPATH:/opt/homebrew/include"

# roswell
export PATH="$PATH:$HOME/.roswell/bin"

# other
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/.ghcup/bin"
export PATH="$PATH:$HOME/go/bin"

chmod +x "$HOME/scripts/*.sh"
export PATH="$PATH:$HOME/scripts"
