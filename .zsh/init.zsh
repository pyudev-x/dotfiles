DOTZSH="$HOME/.zsh"

source $DOTZSH/path.zsh
source $DOTZSH/p10k.zsh
source $DOTZSH/alias.zsh
source $DOTZSH/functions.zsh

export CARGO_NET_GIT_FETCH_WITH_CLI=false
