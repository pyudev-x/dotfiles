export EDITOR="emacs"

alias config="$EDITOR ~/"
alias cls="clear"
alias parrot="curl parrot.live"
alias please="sudo" # be kind to the system
alias dir="ls -la"
alias del="rm -rI"
alias gradle="gradle --console plain"
alias edit='emacs -nw'
