
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp/"))

(require 'plugins)
(require 'lib)

(load (expand-file-name "~/.roswell/helper.el"))
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

(defun add-exec-path (p)
  (add-to-list 'exec-path (expand-file-name p)))

(add-exec-path "/opt/homebrew/bin")
(add-exec-path "~/.cargo/bin")
(add-exec-path "~/.ghcup/bin")
(add-exec-path "~/.roswell/bin")

;; General options
(setq-default make-backup-files nil)
(setq-default auto-save-default nil)
(setq-default create-lockfiles nil)
(setq-default indent-tabs-mode nil)
(setq-default shell-file-name "/bin/zsh")
(setq-default explicit-shell-file-name "/bin/zsh")

(defalias 'yes-or-no-p 'y-or-n-p)
(use-package nerd-icons-dired
  :ensure t
  :straight t
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package treemacs-nerd-icons
  :ensure t
  :straight t
  :config
  (treemacs-load-theme "nerd-icons"))


(tab-bar-mode 1)
(tool-bar-mode 1)
(menu-bar-mode 1)
(xterm-mouse-mode)
(global-hl-line-mode)

;; Theming
(add-to-list 'default-frame-alist '(font . "Iosevka Nerd Font Mono-15"))
(when (display-graphic-p)
  (scroll-bar-mode -1))
(install! autothemer)

(install! modus-themes) ;; The only good themes <- and below
(install! gruber-darker-theme)

;;(load-theme 'gruber-darker t)

;; Language support
(install! rust-mode)
(install! zig-mode)
(install! lua-mode)
(install! haskell-mode)
(install! clojure-mode)
(install! nim-mode)
(install! kotlin-mode)
(install! racket-mode)
(install! cmake-mode)
(install! web-mode)
(require 'lsp)

;; Functions, Macros, and Alias'

(defun user/commit-all (msg)
  (interactive "sEnter a commit message: ")
  (shell-command "git add .")
  (shell-command (concat "git commit -m " msg)))

(defun user/push-origin (branch)
  (interactive "sGit Branch: ")
  (shell-command (concat "git push origin " branch)))

(defun user/term ()
  (interactive)
  (split-window-below)
  (windmove-down)
  (eshell))

(defalias 'qw 'quit-window)
(defalias '++ 'concat)

(defalias 'user/filetree 'dired-sidebar-toggle-with-current-directory)
(defalias 'user/add-project 'treemacs-add-project-to-workspace)
(defalias 'user/color 'load-theme)

;; Keybinds!

(bind-keys*
 :prefix "M-c"
 :prefix-map Keybinds
 ;; Fun (Just some tests actually)
 ("fg" . (lambda ()
          (interactive)
          (message "Hey")))
 ;; Emacs
 ("es" . (lambda (s)
           (interactive "sScript: ")
           (load-file (++ user-emacs-directory "scripts/" s)))))
