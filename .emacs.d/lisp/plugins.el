;; setup straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(defmacro install! (name)
  `(use-package ,name :ensure t :straight t))

;; Basic installs
(install! flycheck)
(install! hydra)
(install! projectile)
(install! which-key)
(install! treemacs)
(install! treemacs-evil)
(install! nerd-icons)
(install! bind-map)
(install! neotree)
(install! dired-sidebar)

;; Use-package
(use-package linum-relative
  :ensure t
  :straight t
  :config
  (global-linum-mode)
  (linum-relative-global-mode))

(use-package yasnippet
  :ensure t
  :straight t
  :config
  (yas-reload-all)
  (yas-global-mode))

(use-package ivy
  :ensure t
  :straight t
  :config
  (ivy-mode))

(use-package company
  :ensure t
  :straight t
  :config
  (add-to-list 'company-backends 'company-capf)
  (global-company-mode))

(use-package evil
  :ensure t
  :straight t
  :config
  (evil-mode 1))

(use-package tree-sitter
  :ensure t
  :straight t
  :config
  (install! tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package elcord
  :ensure t
  :straight t
  :config
  (elcord-mode))

(use-package editorconfig
  :ensure t
  :straight t
  :config
  (editorconfig-mode))

(provide 'plugins)
