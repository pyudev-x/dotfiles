(defalias 'the-hook 'lsp-deferred)

(use-package lsp-mode 
  :straight t
  :hook ((c++-mode rust-mode haskell-mode web-mode) . the-hook)
  :commands lsp)

(use-package lsp-ui
  :straight t
  :after lsp)

(provide 'lsp)
