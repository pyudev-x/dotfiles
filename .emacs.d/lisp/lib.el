;; I am a person who actually codes in Emacs Lisp instead of just using it for Emacs configuration.
;; Lets create some functions for me to use!

(defun greet (name)
  "Basic function to greet NAME"
  (princ (concat "Hello " name "!\n")))

(defmacro repeat (x &body body)
  "Repeat code X times"
  `(dotimes (_ x)
    ,@body))

(defalias 'write 'princ)

(provide 'lib)
